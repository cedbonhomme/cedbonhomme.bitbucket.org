==================
Facial Recognition
==================

To be able to sort the users, and to help them find their lookalike we first had the idea to use **Facial Recognition**.

How it works
============

Eigenfaces
----------

Eigenfaces encode facial features, but also the illumination in the images.

.. image:: assets/eigenfaces_opencv.png


Face reconstruction
~~~~~~~~~~~~~~~~~~~

We can reconstruct a face from its lower dimensional approximation.

.. image:: assets/eigenface_reconstruction_opencv.png

Tutorial on `opencv doc (Eigenfaces)`_

.. _`opencv doc (Eigenfaces)`: http://docs.opencv.org/modules/contrib/doc/facerec/facerec_tutorial.html#eigenfaces

Fisherfaces
-----------

Fisherfaces is less accurate than Eigenfaces. It use a linear approch in the PCA and thus more data is loss than with its counterpart.

.. image:: assets/fisherfaces_opencv.png

Face reconstruction
~~~~~~~~~~~~~~~~~~~

The Fisherfaces allow a reconstruction of the projected image but does not capture illumination as obviously as the Eigenfaces method.

.. image:: assets/fisherface_reconstruction_opencv.png

Tutorial on `opencv doc (Fisherfaces)`_

.. _`opencv doc (Fisherfaces)`: http://docs.opencv.org/modules/contrib/doc/facerec/facerec_tutorial.html#fisherfaces

Principal Component Analysis
----------------------------

Now that we have our faces we want to seperate them into groups, to draw a frontier between genders for example( to know if a new person is a Female or a Male ).
The same could be done with other facial features.

There we have a Tutorial on how to do a `pca lda with gnu octave`_, the example is taken with wine classification.

.. _`pca lda with gnu octave`: http://www.bytefish.de/blog/pca_lda_with_gnu_octave/

OpenCV
======

OpenCV is a free library to use Facial recognition.

OpenCV and QML
--------------

OpenCV libraries are compatible for both Android and iOS.
We use QML to make an awesome mobile application that will be able deploy on both platforms.